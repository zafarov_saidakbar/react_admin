import img403 from '../../assets/images/403.png'
import IMG403 from '../../assets/images/403-img.png'

export default function P403 () {
    return (
        <div className="h-screen flex px-72">
            <div className='h-full w-1/2 flex'>
                <div className='flex flex-col text-left m-auto'>
                    <img src={img403} alt='403' className='object-none mb-8 mr-32' />
                    <h1 className='text-5xl font-bold text-gray-700 mb-10'>Access Forbidden</h1>
                    <p className='text-gray-600 mb-12 text-2xl'>Sorry, the page you visited does not exist.</p>
                    <button className='w-52 rounded text-white bg-blue-500 py-2 px-6'>Вернутся в главную</button> 
                </div>
            </div>
            <div className='h-full w-1/2 flex '>
                <div className='m-auto'>
                    <img src={IMG403} alt='403' />
                </div>
            </div>
        </div>
    )
}