import IMG404 from '../../assets/images/404-img.png'
import img404 from '../../assets/images/404.png'

export default function P404 () {
    return (
        <div className="h-screen flex px-72">
            <div className='h-full w-1/2 flex'>
                <div className='flex flex-col text-left m-auto'>
                    <img src={img404} alt='404' className='object-none mb-8 mr-32' />
                    <h1 className='text-5xl font-bold text-gray-700 mb-10'>Page Not Found</h1>
                    <p className='text-gray-600 mb-12 text-2xl'>Sorry, the page you visited does not exist.</p>
                    <button className='w-52 rounded text-white bg-blue-500 py-2 px-6'>Вернутся в главную</button> 
                </div>
            </div>
            <div className='h-full w-1/2 flex '>
                <div className='m-auto'>
                    <img src={IMG404} alt='404' />
                </div>
            </div>
        </div>
    )
}