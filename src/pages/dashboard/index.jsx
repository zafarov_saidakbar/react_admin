// import { useParams } from 'react-router-dom'
import domik from '../../assets/images/domik-dashboard.svg'
import clients from '../../assets/images/clients-dashboard.svg'
import subscribers from '../../assets/images/subscribers-dashboard.svg'
import couriers from '../../assets/images/couriers-dashboard.svg'
import minus5 from '../../assets/images/minus-5.svg'
import plus25 from '../../assets/images/plus-25.svg'
import plus12 from '../../assets/images/plus-12.svg'
import graph1 from '../../assets/images/graph1.svg'
import graph2 from '../../assets/images/graph2.svg'
import graph3 from '../../assets/images/graph3.svg'
import graph4 from '../../assets/images/graph4.svg'
import graph5 from '../../assets/images/graph5.svg'
import dropDown from '../../assets/images/dropdown-ico.svg'


export default function App () {
    return (
    <div className='flex flex-col w-screen h-full mr-48 ml-3'>
        <div className='flex h-32 mb-5'>
            <div className='w-1/5 bg-white rounded-lg flex justify-between item-center hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                <div className='m-6'>
                    <h1 className='text-2xl font-bold text-light-blue mb-2 text-left'>24</h1>
                    <p className='text-sm text-light-gray'>Филиалы</p>
                </div>
                <div className='m-6 bg-icons-blue rounded-md p-5 w-16 h-16'>
                    <img className='' src={domik} alt='domik' /> 
                </div>
            </div>
            <div className='w-1/5 bg-white rounded-lg flex justify-between item-center hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                <div className='m-6'>
                    <h1 className='text-2xl font-bold text-light-blue mb-2 text-left'>110,823</h1>
                    <p className='text-sm text-light-gray text-left'>Клиенты</p>
                </div>
                <div className='m-6 bg-icons-blue rounded-md p-5 w-16 h-16'>
                    <img src={clients} alt='clients' />
                </div>
            </div>
            <div className='w-1/5 bg-white rounded-lg flex justify-between item-center hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                <div className='m-6'>
                    <h1 className='text-2xl font-bold text-light-blue mb-2 text-left'>110,823</h1>
                    <p className='text-sm text-light-gray'>Подписчики</p>
                </div>
                <div className='m-6 bg-icons-blue rounded-md p-5 w-16 h-16'>
                    <img src={subscribers} alt='subscribers' />
                </div>
            </div>
            <div className='w-1/5 bg-white rounded-lg flex justify-between item-center hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                <div className='m-6'>
                    <h1 className='text-2xl font-bold text-light-blue mb-2 text-left'>75</h1>
                    <p className='text-sm text-light-gray'>Курьеры</p>
                </div>
                <div className='m-6 bg-icons-blue rounded-md p-5 w-16 h-16'>
                    <img src={couriers} alt='couriers' />
                </div>
            </div>
        </div>
        <div className='flex w-full'>
            <div className='w-1/5 flex flex-col mr-5'>
                <div className='text-left w-full bg-white rounded-lg p-5 mb-5 hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                    <p className='text-light-gray text-sm mb-2'>Total orders this month</p>
                    <div className='flex justify-between mb-4'>
                        <h1 className='text-2xl font-bold text-gray-600'>1,850</h1>
                        <img src={minus5} alt='minus5'/>
                    </div>
                    <img src={graph1} alt='graph1' />
                </div>
                <div className='text-left w-full bg-white rounded-lg p-5 mb-5 hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                    <p className='text-light-gray text-sm mb-2'>Total orders this month</p>
                    <div className='flex justify-between mb-4'>
                        <h1 className='text-2xl font-bold text-gray-600'>1,850</h1>
                        <img src={plus25} alt='plus25'/>
                    </div>
                    <img src={graph2} alt='graph2' />
                </div>
                <div className='text-left w-full bg-white rounded-lg p-5  hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer mr-5'>
                    <p className='text-light-gray text-sm mb-2'>Total orders this month</p>
                    <div className='flex justify-between mb-4'>
                        <h1 className='text-2xl font-bold text-gray-600'>1,850</h1>
                        <img src={plus12} alt='plus12'/>
                    </div>
                    <img src={graph3} alt='graph3' />
                </div>
            </div>
            <div className='w-3/5 flex flex-col'>
                <div className='flex w-full mb-5'>
                    <div className='flex w-11/12 text-left mr-5 p-6 bg-white rounded-lg hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer'>
                        <img src={graph4} alt='graph4' />
                        <div className='ml-12 mt-8'>
                            <p className='text-light-gray text-sm mb-2'>Top Order</p>
                            <h1 className='text-4xl text-light-green font-bold'>75%</h1>
                        </div>
                    </div>
                    <div className='flex w-11/12 text-left p-6 bg-white rounded-lg hover:drop-shadow-2xl hover:-translate-y-1 hover:scale-110 hover: duration-150 cursor-pointer'>
                        <img src={graph5} alt='graph5' />
                        <div className='ml-12 mt-8 w-full '>
                            <p className='text-light-gray text-sm mb-2'>Average order</p>
                            <h1 className='text-4xl text-purple font-bold'>40%</h1>
                        </div>
                    </div>
                </div>
                <div className='flex flex-col bg-white rounded-lg'>
                    <div className='flex items-center justify-between py-4 px-5'>
                        <p className='font-semibold text-lg text-gray-500'>Ежемесячная статистика</p>
                        <button className='flex items-center w-40 justify-between border border-very-light-grey rounded-lg py-2 px-4'>
                            <p className='text-sm text-dark-gray'>Июнь</p>
                            <img src={dropDown} alt='dropdown' />
                        </button>
                        {/* todo dropdown list */}
                    </div>
                    <div>

                    </div>
                    <div className='flex justify-center border-t border-very-light-gray text-light-gray'>
                        <div className='py-4 px-6'>
                            <p>Неделья</p>
                        </div>
                        <div className='p-4 px-8'>
                            <p>Месяц</p>
                        </div>
                        <div className='p-4 px-8'>
                            <p>Год</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}