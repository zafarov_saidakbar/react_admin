import { useParams } from 'react-router-dom'
export default function App () {
    const param = useParams()
    console.log(param)
    return (<div>
        <h2>Hello world from { param.id }</h2>
    </div>)
}