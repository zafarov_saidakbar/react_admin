import { Outlet } from 'react-router-dom'

import Navbar from '@layouts/Navbar'
import Sidebar from '@layouts/Sidebar'
// import Wrapper from '@layouts/Wrapper'

export default function MainContainer () {
    return (
        <div className="fixed flex bg-white">
            <div className="flex w-16 h-full">
                <Sidebar />
            </div>
            <div className="shrink w-full h-full">
                <Navbar />
                <div className="flex-none bg-dashboard-blue h-screen w-screen p-6">{ <Outlet/>}</div>
            </div>
        </div>
    )
}