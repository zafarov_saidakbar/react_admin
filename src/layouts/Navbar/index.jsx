
export default function App ({ children }) {
    return (
        <section className="border border-slate-100 h-12 flex item-center w-screen">
            <h1 className="font-semibold text-2xl pl-8 pt-2 text-[#303940]">Dashboard</h1>
        </section>
    )
}