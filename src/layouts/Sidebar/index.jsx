import logo from '../../assets/images/logo-sidebar.png'
import ToDoNav from '../../assets/images/todo-navigation.png'
import bell from '../../assets/images/notification-sidebar.svg'
import avatar from '../../assets/images/avatar-sidebar.png'

export default function App ({ children }) {
    return (
        <div className="flex flex-col justify-between flex-end w-16 item-center h-screen border border-slate-100">
            <div className='mt-3 ml-3'>
                <img src={logo} alt="logo" className="w-10 mb-6" />
                <img src={ToDoNav} alt='todo' className='cursor-pointer' />
            </div>
            
            <div className='flex flex-col '>
                <button className='mb-6 m-auto'>
                    <img src={bell} alt='bell' />
                </button>

                <button className='m-auto mb-4'>
                    <img src={avatar} alt='avatar' />
                </button>   
            </div>

        </div>
    )
}