module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
      },
      colors: {
        'dashboard-blue': '#dce9f9',
        'light-blue': '#4094f7',
        'light-gray': '#6E8BB7',
        'light-green': '#1AC19D',
        'purple': '#A23FEE',
        'very-light-gray': '#E5E9EB',
        'dark-gray': '#303940',
      },
      backgroundColor: {
        'icons-blue': 'rgba(64, 148, 247, 0.15)',
      },
    },
  },
  plugins: [],
}
