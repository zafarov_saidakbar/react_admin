const path = require('path');

const resolvePath = p => path.resolve(__dirname, p)

module.exports = {
    webpack: {
        alias: {
            '@components': resolvePath('./src/components'),
            '@assets': resolvePath('./src/assets'),
            '@services': resolvePath('./src/services'),
            '@containers': resolvePath('./src/containers'),
            '@docs': resolvePath('./src/docs'),
            '@locales': resolvePath('./src/locales'),
            '@routers': resolvePath('./src/routers'),
            '@redux': resolvePath('./src/redux'),
            '@utils': resolvePath('./src/utils'),
            '@layouts': resolvePath('./src/layouts'),
            '@configs': resolvePath('./src/configs'),
            '@pages': resolvePath('./src/pages'),
        }
    }
}